<?php

session_start();

if (!isset($_SESSION['username'])) {
  echo "<script>
  alert('Login terlebih dahulu!');
  window.location.href = 'login.php';
  </script>";
}
?>

<!DOCTYPE html>
<html lang="en" dir="ltr">
<head>
  <meta charset="utf-8">
  <title>Real Harvest Moon</title>
  <link rel="stylesheet" href="style.css">
  <link href="https://fonts.googleapis.com/css?family=Roboto&display=swap" rel="stylesheet">

</head>
<body>
  <div class="container home">
    <nav class="navbar">
      <ul>
        <li><a href="home.php">Home</a></li>
        <li><a href="jenis-hewan.php">Jenis Hewan</a></li>
        <li><a href="tips-beternak.php">Tips Beternak</a></li>
        <li class="dropdown" style="float:right">
          <button class="dropbtn"><a href="#">Welcome,<?php echo $_SESSION['username'];  ?></a>
          </button>
          <div class="dropdown-content">
            <a href="logout.php">Logout</a>
            <a href="profile.php">See Profile</a>
            <a href="author.php">See Author</a>
          </div>
        </li>
      </ul>
    </nav>

    <div class="konten" style="color: black">
    <h2>Tips Beternak</h2>
      <p>Dalam berternak ada beberapa hal penting yang harus kita perhatikan. Pertama kita akan
        membahas hal yang umum dalam berternak yaitu:</p>
        <ol>
          <li><b>Kita harus memiliki rumah atau kandang untuk hewan ternak kita.</b></li>
          <p>
            Hal ini dikarenakan setiap hewan ternak butuh perlindungan akan cuaca, malah hari, suhu, dll
            sehingga hewan ternak seperti sapi butuh kandang yang cocok untuk mereka beristirahat dan
            berlindung.
          </p>
          <li><b>Hewan ternak harus terkena sinar matahari setiap harinya.</b></li>
          <p>
            Agar terhindar dari kuman dan penyakit, hewan ternak setiap harinya harus terkena sinar matahari
            maupun dari dalam kandang atau dari luar. Jika di luar, maka direkomendasikan untuk ditaruh
            dilapangan yang dipagari (agar tidak kabur) hal ini juga akan menghemat biaya dalam pemberian
            pakan karena hewan ternak akan mencari makan sendiri.
          </p>
          <li><b>Hewan ternak harus mendapatkan makanan dan air minum yang cukup setiap harinya.</b></li>
          <p>
            Tentunya hewan ternak merupakan mahluk hidup yang membutuhkan makanan dan minuman.
            Maka dari itu pemberian pakan juga harus cukup tidak boleh lebih dan kurang karena hal tersebut
            akan mempengaruhi mood binatang dan akan mempengaruhi hasil ternak.
          </p>
          <li><b>Mencari pakan alternative.</b></li>
          <p>
            Hal ini biasanya cukup disepelekan oleh peternak pemula. Seperti yang kita ketahui, pakan ternak
            tidaklah murah. Bisa dikatakan murah apabila kita membelinya untuk 1 atau 2 hewan. Tapi
            bagaiamana jadinya kalau hewan ternak kita ada 50 lebih hal tersebut akan sangat boros
            dikarenakan hewan ternak akan butuh makan setiap hari. Hal tersebuta akan mengurangi
            keuntungan yang kita dapatkan. Pakan alternative dapat kita dapatkan dengan menyisihkan
            tanah yang kita miliki dengan membuat lapangan berumput sehingga sapi dapat mencari makan
            sendiri dan menanam beberapa sayur untuk pakan ayam seperti jagung, dll.
          </p>
          <li><b>Perawatan hewan ternak.</b></li>
          <p>
            Hewan ternak juga butuh kasih sayang dimana kita harus lembut dalam menangani hewan-hewan
            ternak agar mood dari hewan ternak dapat dipertahankan sehingga hasil ternak dapat meningkat.
          </p>
        </ol>
    </div>

    <div class="konten" style="color: black">
      <table class="table table-bordered mx-auto mt-5" style="width: 58rem;">
    <thead>
      <tr>
        <th>No</th>
        <th>Title</th>
        <th>Content</th>
        <th>Author</th>
        <th>Status</th>
      </tr>
    </thead>
    <tbody>
      <?php 
        $koneksi = mysqli_connect('localhost','root','','harvestmoon');
        $idUser = $_SESSION['username'];
        $isi = "SELECT * FROM `tips`";
        $query = mysqli_query($koneksi, $isi); 
        $price = 0;
      if (mysqli_num_rows($query) > 0) {
        $i = 1;
        while ($hasil = mysqli_fetch_assoc($query)) {
        ?>

        <tr>
            <th><?php echo $i; $i++;?></th>
            <td><?php echo $hasil["title"]; ?></td>
            <td><?php echo $hasil["content"]; ?></td>
            <td><?php echo $hasil["author"];?></td>
            <td><a href="deletetips.php?id=<?php echo $hasil["id"]; ?>">hapus</a></td>
        </tr>

        <?php }
      } 
        ?>
    </tbody>
  </table>
      
            <div style="clear:both" align="center">
        <h2>Ingin menambahkan? </h2>
        <a href="tips2.php"><button class="button-utama">Tambah Tips</button></a>
            </div>
        </ul>
      </div>
    </div>
    <br>
    <br>
    <br>
     
  </body>
  <link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
  <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
</html>