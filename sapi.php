<?php

session_start();

if (!isset($_SESSION['username'])) {
  echo "<script> alert('Login terlebih dahulu!');
  window.location.href = 'login.php';
  </script>";
}

?>


<!DOCTYPE html>
<html lang="en" dir="ltr">
<head>
  <meta charset="utf-8">
  <title>Real Harvest Moon</title>
  <link rel="stylesheet" href="style.css">
  <link href="https://fonts.googleapis.com/css?family=Roboto&display=swap" rel="stylesheet">

</head>
<body>
  <div class="container home">
    <nav class="navbar">
      <ul>
        <li><a href="home.php">Home</a></li>
        <li><a href="jenis-hewan.php">Jenis Hewan</a></li>
        <li><a href="tips-beternak.php">Tips Beternak</a></li>
        <li class="dropdown" style="float:right">
          <button class="dropbtn"><a href="#">Welcome,<?php echo $_SESSION['username'];  ?></a>
          </button>
          <div class="dropdown-content">
            <a href="logout.php">Logout</a>
            <a href="profile.php">See Profile</a>
            <a href="author.php">See Author</a>
          </div>
        </li>
      </ul>
    </nav>
    <div class="konten">
      <div class="gambar-hewan">
        <a href="sapi.php"><img src="image/sapi.png" alt="Gambar Sapi" height="200" width="200"></a>
      </div>
      <h2>Sapi</h2>
      <p>
        Sapi, merupakan salah satu hewan ternak yang paling umum dikarenakan sapi menghasilkan
        Susu dan Daging yang merupakan bahan makanan sehari-hari yang biasa digunakan.
        <br><br>
        Sapi sendiri memiliki 2 jenis yaitu Sapi Pedaging dan Sapi Perah. Untuk saat ini kita
        akan membahas jenis sapi perah terlebih dahulu.
        <br><br>
        Jenis Sapi Perah yang umum digunakan di Indonesia:
        <br>
        <ol>
        <li>Sapi FH</li>
        <img src="image/FH.png" alt="Sapi FH" height="300">
        <p>
        Sapi FH merupakan jenis sapi perah di Indonesia yang  sangat populer sebagai
        sapi perah. Pertama dibawa dari pulau Fries Land barat Belanda dan sebagian dari
        Australia serta Selandia baru, Amerika, Kanada, dan Jepang. Warnanya putih dan
        hitam dan sangat disukai peternak.
        <br><br>
        Sapi FH memiliki performansi yang baik sebagai penghasil daging dan susu.
        Distribusinya sebagian di dataran tinggi (700 m di atas permukaan laut) dengan
        temperatur antara 16-23o C, lembab dan basah di pulau Jawa. Sapi Holsteins dapat
        dikenali dengan cepat dari warnanya yaitu putih dan hitam/merah serta produksi
        susunya yang tinggi. Berat pedet yang baru lahir dapat mencapai 45 kg, berat dewasa
        dapat mencapai 750 kg dengan tinggi 58 inchi.
        </p>
        <li>Sapi Jersey</li>
        <img src="image/jersey.jpg" alt="Sapi Jersey" height="300">
        <p>
        Sapi Jersey berasal dari pulau Jersey di Inggris, digunakan sebagai penghasil susu.
        Ukuran sapi kecil berkisar 360 sampai 540 kg untuk sapi betina dan 540 sd 820 kg
        untuk sapi pejantan. Kandungan lemak susu pada susu sapi jersey tinggi. Jenis sapi
        ini belum ada di Indonesia. Warna sapi bervariasi dari abu-abu terang sampai hitam.
        Paha, kepala dan bahu sapi warnanya lebih gelap daripada warna tubuhnya.
        </p>
        <li>Sapi Grati</li>
        <img src="image/grati.jpg" alt="Sapi FH" height="300">
        <p>
        Sapi grati merupakan hasil persilangan sapi FH dengan sapi Jawa-ongole.
        Sapi Grati dikembangkan di dataran rendah di daerah Grati, Jawa Timur. Populasi
        sapi Grati sekitar 10.000 ekor.
        </p>
        </ol>
      </p>
    </div>
  </div>
</body>
</html>
