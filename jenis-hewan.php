<?php

session_start();

if (!isset($_SESSION['username'])) {
  echo "<script>
  alert('Login terlebih dahulu!');
  window.location.href = 'login.php';
  </script>";
}

?>

<!DOCTYPE html>
<html lang="en" dir="ltr">
<head>
  <meta charset="utf-8">
  <title>Real Harvest Moon</title>
  <link rel="stylesheet" href="style.css">
  <link href="https://fonts.googleapis.com/css?family=Roboto&display=swap" rel="stylesheet">

</head>
<body>
  <div class="container home">
    <nav class="navbar">
      <ul>
        <li><a href="home.php">Home</a></li>
        <li><a href="jenis-hewan.php">Jenis Hewan</a></li>
        <li><a href="tips-beternak.php">Tips Beternak</a></li>
        <li class="dropdown" style="float:right">
          <button class="dropbtn"><a href="#">Welcome,<?php echo $_SESSION['username'];  ?></a>
          </button>
          <div class="dropdown-content">
            <a href="logout.php">Logout</a>
            <a href="profile.php">See Profile</a>
            <a href="author.php">See Author</a>
          </div>
        </li>
      </ul>
    </nav>
    <div class="konten">
      <h2>Pilih Jenis Hewan Yang Mau Dipelajari</h2>
      <div class="gambar-hewan">
        <a href="sapi.php"><img src="image/sapi.png" alt="Gambar Sapi" height="200" width="200"></a>
        <h2 style="text-align:center">Sapi</h2>
      </div>
      <div class="gambar-hewan">
        <a href="ayam.php"><img src="image/ayam.png" alt="Gambar Ayam" height="200" width="200"></a>
        <h2 style="text-align:center">Ayam</h2>
      </div>
      <div class="kanan" style="clear:both">
        <h2>Sudah Mempelajari ? </h2>
        <a href="simulasi.php"><button class="button-utama">Mulai Simulasi</button></a>
      </div>
    </div>
  </div>
</body>
</html>

