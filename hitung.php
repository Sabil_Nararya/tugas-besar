<?php

session_start();

if (!isset($_SESSION['username'])) {
  echo "<script>
  alert('Login terlebih dahulu!');
  window.location.href = 'login.php';
  </script>";
}

?>

<?php
include "connection.php";

$jumlah_sapi = $_POST['jumlah_sapi'];
$jumlah_ayam = $_POST['jumlah_ayam'];
$dana_awal = $_POST['dana_awal'];
$durasi = $_POST['durasi'];

$pakan_sapi = 250000;
$pakan_ayam = 320000;

$jenis_sapi = $_POST['jenis_sapi'];
$keuntungan_sapi;
if ($jenis_sapi == "FH") {

  $total_pakan = ceil($jumlah_sapi/5)*250000*($durasi*4);
  $keuntungan_sapi = (12*($durasi*30)*7500*$jumlah_sapi) - $total_pakan;

}else if($jenis_sapi == "Jersey"){

  $total_pakan = ceil($jumlah_sapi/5)*250000*($durasi*4);
  $keuntungan_sapi = ((12*($durasi*30))*9000*$jumlah_sapi) - $total_pakan;

}else if($jenis_sapi == "Grati"){

  $total_pakan = ceil($jumlah_sapi/5)*250000*($durasi*4);
  $keuntungan_sapi = (12*($durasi*30)*8000*$jumlah_sapi) - $total_pakan;

}else if($jenis_sapi == "Ayrshire"){
  $total_pakan = ceil($jumlah_sapi/5)*250000*($durasi*4);
  $keuntungan_sapi = ((12*($durasi*30))*7000*$jumlah_sapi) - $total_pakan;

}else{

  $total_pakan = ceil($jumlah_sapi/5)*250000*($durasi*4);
  $keuntungan_sapi = (12*($durasi*30)*6500*$jumlah_sapi) - $total_pakan;

}

$jenis_ayam = $_POST['jenis_ayam'];
$keuntungan_ayam;
if ($jenis_ayam == "Hibrida") {

  $total_pakan = ceil($jumlah_ayam/100)*320000*$durasi;
  $keuntungan_ayam = (($durasi*30)*1200*$jumlah_ayam) - $total_pakan;

}else if($jenis_ayam == "Leghorn"){

  $total_pakan = ceil($jumlah_ayam/100)*320000*$durasi;
  $keuntungan_ayam = (($durasi*30)*1700*$jumlah_ayam) - $total_pakan;

}else{

  $total_pakan = ceil($jumlah_ayam/100)*320000*$durasi;
  $keuntungan_ayam = (($durasi*30)*1400*$jumlah_ayam) - $total_pakan;

}

$total_keuntungan = ($keuntungan_ayam + $keuntungan_sapi) - $dana_awal;

$query = mysqli_query($conn, "INSERT INTO history VALUES('', 'Simulasi Berhasil Dilakukan', '$total_keuntungan')");
?>

<!DOCTYPE html>
<html lang="en" dir="ltr">
<head>
  <meta charset="utf-8">
  <title>Real Harvest Moon</title>
  <link rel="stylesheet" href="style.css">
  <link href="https://fonts.googleapis.com/css?family=Roboto&display=swap" rel="stylesheet">

</head>
<body>
  <div class="container home">
    <nav class="navbar">
      <ul>
        <li><a href="home.php">Home</a></li>
        <li><a href="jenis-hewan.php">Jenis Hewan</a></li>
        <li><a href="tips-beternak.php">Tips Beternak</a></li>
        <li class="dropdown" style="float:right">
          <button class="dropbtn"><a href="#">Welcome,<?php echo $_SESSION['username'];  ?></a>
          </button>
          <div class="dropdown-content">
            <a href="logout.php">Logout</a>
            <a href="profile.php">See Profile</a>
            <a href="author.php">See Author</a>
          </div>
        </li>
      </ul>
    </nav>
    <div class="konten"> 
      <h1>Hasil Simulasi</h1>
      <div class="paket">
        <label for="">Hasil Ternak</label><br>
        <h2 name="UangUntung">Rp <?php echo number_format($total_keuntungan,2,",","."); ?></h2>
      </div>
      <div class="paket">
        <label for="">Kesimpulan</label><br>
        <?php if ($total_keuntungan >= 0): ?>
          <p>Anda sudah mendapatkan keuntungan dari simulasi yang anda lakukan</p>
        <?php else: ?>
          <p>Jangka Waktu Ternak Kurang Lama Tambah Jumlah Hewan atau Jangka Waktu Ternak</p>
        <?php endif; ?>

      </div>
      <a href="simulasi.php"><button class="button-utama">Simulasi Lagi</button></a>
      <a href="History.php"><button class="button-utama">Cek History</button></a>
    </div>
  </div>

</body>
</html>
