<!DOCTYPE html>
<html lang="en" dir="ltr">
<head>
  <meta charset="utf-8">
  <title>Real Harvest Moon</title>
  <link rel="stylesheet" href="style.css">
  <link href="https://fonts.googleapis.com/css?family=Roboto&display=swap" rel="stylesheet">

</head>
<body>
  <div class="container home">
    <nav class="navbar">
      <ul>
        <li><a href="home.php">Home</a></li>
        <li><a href="jenis-hewan.php">Jenis Hewan</a></li>
        <li><a href="tips-beternak.php">Tips Beternak</a></li>
        <li class="dropdown" style="float:right">
          <button class="dropbtn"><a href="#"></a>
          </button>
          <div class="dropdown-content">
            <a href="logout.php">Logout</a>
            <a href="profile.php">See Profile</a>
            <a href="author.php">See Author</a>
          </div>
        </li>
      </ul>
    </nav>
    <div align= "center">
            <h1>Ayam</h1>
            <form action="" method="POST">
                <table>
                    <tr>
                        <td>
                            <label>Jenis ayam</label>
                        </td>
                        <td><input type="text" name="jenis_ayam" value="<?php echo $_SESSION['jenis_ayam']; ?>">      
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label>Nama ayam</label>
                        </td>
                        <td><input type="text" name="nama_ayam" value="<?php echo $_SESSION['nama_ayam']; ?>">
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label>Dekripsi</label>
                        </td>
                        <td><input type="text" name="dekripsi" value="<?php echo $_SESSION['dekripsi']; ?>">
                        </td>
                    </tr>
                    <tr>
                        <td><hr></td>
                        <td><hr></td>
                    </tr>
                    <tr>
                </table>
                <br>
                <button type="submit" class="save">Save</button>
                <br>
                <button type="button" class="cancel" onclick="window.location.href=''">Cancel</button>
            </form>
        </div>