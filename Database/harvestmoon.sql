-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Nov 26, 2019 at 03:49 AM
-- Server version: 10.4.6-MariaDB
-- PHP Version: 7.3.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `harvestmoon`
--

-- --------------------------------------------------------

--
-- Table structure for table `history`
--

CREATE TABLE `history` (
  `id` int(11) NOT NULL,
  `simulasi` varchar(200) NOT NULL,
  `keuntungan` int(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `history`
--

INSERT INTO `history` (`id`, `simulasi`, `keuntungan`) VALUES
(6, 'Simulasi Berhasil Dilakukan', 219480000),
(7, 'Simulasi Berhasil Dilakukan', 87800000),
(0, 'Simulasi Berhasil Dilakukan', 288560000),
(0, 'Simulasi Berhasil Dilakukan', 2147483647);

-- --------------------------------------------------------

--
-- Table structure for table `post_ayam`
--

CREATE TABLE `post_ayam` (
  `id` int(11) NOT NULL,
  `post` text NOT NULL,
  `image` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `post_ayam`
--

INSERT INTO `post_ayam` (`id`, `post`, `image`) VALUES
(3, 'Silahkan isi komentar anda', 'Background Farm.jpg'),
(4, 'Silahkan isi komentar anda', 'Background Farm.jpg'),
(6, 'Ayam White Leghorn\r\n\r\nAyam ini merupakan tipe dari ayam ....', 'FB cover.jpg'),
(7, 'Ayam White Leghorn \r\n\r\nmerupakan ayam yang berasal dari keluarga...', '');

-- --------------------------------------------------------

--
-- Table structure for table `post_sapi`
--

CREATE TABLE `post_sapi` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `post` text DEFAULT NULL,
  `image` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `post_tips`
--

CREATE TABLE `post_tips` (
  `id` int(20) NOT NULL,
  `post` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tips`
--

CREATE TABLE `tips` (
  `id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `content` varchar(255) NOT NULL,
  `author` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tips`
--

INSERT INTO `tips` (`id`, `title`, `content`, `author`) VALUES
(3, 'Menyusui sapi', 'cara menyusui sapi, adalah memberinya susu', 'Idu si ganteng'),
(4, 'Cara menggendong sap', 'Jangan gendong sendiri, berat, biar aku saja', 'idu lagi'),
(5, 'udah ah capek', 'iya gtu aja', 'masih idu'),
(6, 'futari ecchi', 'gaboleh ya', 'bukan idu');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id` int(10) NOT NULL,
  `username` varchar(20) NOT NULL,
  `password` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `username`, `password`) VALUES
(6, 'farhan', '123'),
(7, 'idu', 'idu');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tips`
--
ALTER TABLE `tips`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tips`
--
ALTER TABLE `tips`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
