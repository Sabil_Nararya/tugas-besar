<?php

session_start();

if (!isset($_SESSION['username'])) {
  echo "<script>
  alert('Login terlebih dahulu!');
  window.location.href = 'login.php';
  </script>";
}

?>
<!DOCTYPE html>
<html lang="en" dir="ltr">
<head>
  <meta charset="utf-8">
  <title>Real Harvest Moon</title>
  <link rel="stylesheet" href="style.css">
  <link href="https://fonts.googleapis.com/css?family=Roboto&display=swap" rel="stylesheet">

</head>
<body>
  <div class="container home">
    <nav class="navbar">
      <ul>
        <li><a href="home.php">Home</a></li>
        <li><a href="jenis-hewan.php">Jenis Hewan</a></li>
        <li><a href="tips-beternak.php">Tips Beternak</a></li>
        <li class="dropdown" style="float:right">
          <button class="dropbtn"><a href="#">Welcome,<?php echo $_SESSION['username'];  ?></a>
          </button>
          <div class="dropdown-content">
            <a href="logout.php">Logout</a>
            <a href="profile.php">See Profile</a>
            <a href="author.php">See Author</a>
          </div>
        </li>
      </ul>
      <h2>HISTORY</h2>
        <table border="1" align="center">
        <tr>
            <th>No</th>
            <th>Dana Awal</th>
            <th>Keuntungan</th>
        </tr>
        <?php
@include "connection.php";

$sql = "SELECT id, simulasi, keuntungan FROM history";
$result = mysqli_query($conn, $sql);

if (mysqli_num_rows($result) > 0) {
    // output data of each row
    $i=0;
    while($row = mysqli_fetch_assoc($result)) {
        $i++;
        echo "<tr>
        <td>". $row["id"]. "</td>
        <td>". $row["simulasi"]. "</td>
        <td>". $row["keuntungan"]."</td>
        </tr>";
    }
} else {
    echo "0 results";
}
?>
    </nav>
  </div>
  

</body>
</html>