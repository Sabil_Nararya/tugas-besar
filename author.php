<?php

session_start();

if (!isset($_SESSION['username'])) {
  echo "<script>
  alert('Login terlebih dahulu!');
  window.location.href = 'login.php';
  </script>";
}

?>

<!DOCTYPE html>
<html lang="en" dir="ltr">
<head>
  <meta charset="utf-8">
  <title>See Author</title>
  <link rel="stylesheet" href="style.css">
  <link href="https://fonts.googleapis.com/css?family=Roboto&display=swap" rel="stylesheet">

  </head>
    <body>
    	<div class="container home">
    <nav class="navbar">
      <ul>
        <li><a href="home.php">Home</a></li>
        <li><a href="jenis-hewan.php">Jenis Hewan</a></li>
        <li><a href="tips-beternak.php">Tips Beternak</a></li>
        <li class="dropdown" style="float:right">
          <button class="dropbtn"><a href="#">Welcome,<?php echo $_SESSION['username'];  ?></a>
          </button>
          <div class="dropdown-content">
            <a href="logout.php">Logout</a>
            <a href="profile.php">See Profile</a>
            <a href="author.php">See Author</a>
          </div>
        </li>
      </ul>
    </nav>


    <div class="konten" >
      <h1 style="text-align: center;">Author</h1>
      <div class="gambar-hewan">
        <img src="image/sapi.png" alt="Gambar Sapi" height="200" width="200">
        <h2 style="text-align:center">Sabil Nararya</h2>
      </div>
      <div class="gambar-hewan">
        <img src="image/ayam.png" alt="Gambar Ayam" height="200" width="200">
        <h2 style="text-align:center">Kusumah Anggraito</h2>
      </div>
      <div class="gambar-hewan">
        <img src="image/sapi.png" alt="Gambar Sapi" height="200" width="200">
        <h2 style="text-align:center">Robby Alexander</h2>
      </div>
      <div class="gambar-hewan">
        <img src="image/ayam.png" alt="Gambar Ayam" height="200" width="200">
        <h2 style="text-align:center">Yudha Alfarid</h2>
      </div>
      <div class="gambar-hewan">
        <img src="image/sapi.png" alt="Gambar Sapi" height="200" width="200">
        <h2 style="text-align:center">Farhan Hasan</h2>
      </div>
      <div class="gambar-hewan">
        <img src="image/ayam.png" alt="Gambar Ayam" height="200" width="200">
        <h2 style="text-align:center">Jessy Anugerah</h2>
      </div>
      <br>
   </div> 	
      <div>
      	<br><br><br><br><br><br><br><br>
      <h1 style="text-align: center;">The Real Harvest Moon Untuk memenuhi tugas besar WAD SI-41-07</h1>
  	  </div>
 </body>
</html>
