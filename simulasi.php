<?php

session_start();

if (!isset($_SESSION['username'])) {
  echo "<script>
  alert('Login terlebih dahulu!');
  window.location.href = 'login.php';
  </script>";
}

?>


<!DOCTYPE html>
<html lang="en" dir="ltr">
<head>
  <meta charset="utf-8">
  <title>Real Harvest Moon</title>
  <link rel="stylesheet" href="style.css">
  <link href="https://fonts.googleapis.com/css?family=Roboto&display=swap" rel="stylesheet">

</head>
<body>
  <div class="container home">
    <nav class="navbar">
      <ul>
        <li><a href="home.php">Home</a></li>
        <li><a href="jenis-hewan.php">Jenis Hewan</a></li>
        <li><a href="tips-beternak.php">Tips Beternak</a></li>
        <li class="dropdown" style="float:right">
          <button class="dropbtn"><a href="#">Welcome,<?php echo $_SESSION['username'];  ?></a>
          </button>
          <div class="dropdown-content">
            <a href="logout.php">Logout</a>
            <a href="profile.php">See Profile</a>
            <a href="author.php">See Author</a>
          </div>
        </li>
      </ul>
    </nav>
    <div class="konten">
      <h1>Simulasi</h1>
      <form class="" action="hitung.php" method="post">
        <div class="paket" style="width:400px;float:left;">
          <label for="">Masukan Jenis Sapi</label><br>
          <select class="input-utama" name="jenis_sapi" required>
            <option value="FH">FH</option>
            <option value="Jersey">Jersey</option>
            <option value="Grati">Grati</option>
            <option value="Ayrshire">Ayrshire</option>
            <option value="Ongole">Ongole</option>
          </select>
        </div>
        <div class="paket" style="width:400px;float:right;">
          <label for="">Masukan Durasi (Bulan)</label><br>
          <input type="text" name="durasi" value="" class="input-utama" required>
        </div>
        <div class="paket" style="width:400px;clear:both;">
          <label for="">Masukan Quantity</label><br>
          <input type="text" name="jumlah_sapi" value="" class="input-utama" required>
        </div>
        <div class="paket" style="width:400px;">
          <label for="">Masukan Jenis Ayam</label><br>
          <select class="input-utama" name="jenis_ayam" required>
            <option value="Hibrida">Hibrida</option>
            <option value="Leghorn">White Leghorn</option>
            <option value="Browman">Lohman Browman</option>
          </select>
        </div>
        <div class="paket" style="width:400px;">
          <label for="">Masukan Quantity</label><br>
          <input type="text" name="jumlah_ayam" value="" class="input-utama" required>
        </div>
        <center>
          <button class="button-utama" type="button" onclick="hitung()">Submit</button>
        </center>
        <div class="paket" style="width:400px;">
          <label for="">Dana Awal yang Dibutuhkan</label><br>
          <input type="text" name="dana_awal" value="0" class="input-utama" required>
        </div>
        <button class="button-utama" type="submit" style="float:right;">Mulai Simulasi</button>
      </form>

    </div>
  </div>

  <script src="https://code.jquery.com/jquery-3.4.1.min.js"></script>
  <script type="text/javascript">
  function hitung(){
    var jenis_sapi = $("select[name='jenis_sapi']").val();
    var harga_sapi;
    if (jenis_sapi == "FH") {
      harga_sapi = 5000000;
    }else if(jenis_sapi == "Jersey"){
      harga_sapi = 6000000;
    }else if(jenis_sapi == "Grati"){
      harga_sapi = 5500000;
    }else if(jenis_sapi == "Ayrshire"){
      harga_sapi = 6250000;
    }else{
      harga_sapi = 5200000;  
    }
    
    var jumlah_sapi = $("input[name='jumlah_sapi']").val();

    var jenis_ayam = $("select[name='jenis_ayam']").val();
    var harga_ayam;
    if (jenis_ayam == "Hibrida") {
      harga_ayam = 8000;
    }else if(jenis_ayam == "Leghorn"){
      harga_ayam = 9000;
    }else{
      harga_ayam = 8500;
    }
    var jumlah_ayam = $("input[name='jumlah_ayam']").val();

    var modal_awal = (harga_sapi * jumlah_sapi) + (harga_ayam * jumlah_ayam);

    $("input[name='dana_awal']").attr('value', modal_awal);
  }
  </script>

</body>
</html>
