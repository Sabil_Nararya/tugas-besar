<?php

session_start();

if (!isset($_SESSION['username'])) {
  echo "<script>
  alert('Login terlebih dahulu!');
  window.location.href = 'login.php';
  </script>";
}

?>

<!DOCTYPE html>
<html lang="en" dir="ltr">
<head>
  <meta charset="utf-8">
  <title>Real Harvest Moon</title>
  <link rel="stylesheet" href="style.css">
  <link href="https://fonts.googleapis.com/css?family=Roboto&display=swap" rel="stylesheet">

</head>
<body>
  <div class="container home">
    <nav class="navbar">
      <ul>
        <li><a href="home.php">Home</a></li>
        <li><a href="jenis-hewan.php">Jenis Hewan</a></li>
        <li><a href="tips-beternak.php">Tips Beternak</a></li>
        <li class="dropdown" style="float:right">
          <button class="dropbtn"><a href="#">Welcome,<?php echo $_SESSION['username'];  ?></a>
          </button>
          <div class="dropdown-content">
            <a href="logout.php">Logout</a>
            <a href="profile.php">See Profile</a>
            <a href="author.php">See Author</a>
          </div>
        </li>
      </ul>
    </nav>
    <div class="konten">
      <div class="gambar-hewan">
        <a href="sapi.php"><img src="image/ayam.png" alt="Gambar Ayam" height="200" width="200"></a>
      </div>
      <h2>Ayam</h2>
      <p>
        Ayam adalah hewan yang termasuk dalam kelas aves (hewan bersayap) yang mudah
        beradaptasi di segala tempat. Jenis ayam sendiri ada 3 yaitu Ptelur, Pedaging, dan
        Penyanyi. Untuk saat ini kita akan membahas jenis ayam petelur terlebih dahulu.
        <br><br>
        Jenis jenis ayam petelur di Indonesia:
        <br>
        <ol>
          <li>Ayam Ras Petelur Coklat (Hibrida)</li>
          <img src="image/hibrida.jpg" alt="Hibrida" height="300">
          <p>
            Ayam Hibrida terbagi menjadi beberapa jenis tetapi ayam hibrida yang banyak
            dikenal adalah jenis Golden Comet yang dikenal sebagai ayam negeri atau ayam
            ras petelur. Bobot ayam poetelu dapat mencapai sekitar 2 kg, meskipun tidak seberat
            dengan ayam broiler tetapi daging yang dihasilakan oleh ayam petelur memiliki rasa
            daging yang lebih enak dibanding dengan daging ayam pedaging broiler.
            <br><br>
            Jenis ayam hibrida tersebut memiliki sifat mengkonsumsi pakannya yang sedikit,
            tetapi dengan dayam konsumsi pakan yang sedikit ayam petelur hibrida ini dapat
            menghasilkan telur dengan jumlah yang banyak. Oleh karena itu, ayam ini terbilang
            memiliki harga lebih murah dibanding dengan breed lain.
            <br><br>
            Di wilayah Indonesia sendiri, para peternak banyak memilih ayam petelur hibrida
            untuk dibudidayakan. Tubuh ayam hibrida memiliki warna emas dan coklat dengan
            ekornya yang berwarna putih. Ayam petelur hibrida dapat bertelur sebanyak 280 an
            butir setiap tahunnya dengan warna telur cokelat dan ukurannya yang sedang.
          </p>
          <li>Ayam Petelur Putih (White Leghorn)</li>
          <img src="image/leghorn.jpg" alt="Leghorn" height="300">
          <p>
            Ayam White Leghorn atau biasa disebut ayam Petelur Putih merupakan ayam ras
            petelur yang memiliki warna bulu putih bersih dan telu ryang dihasilkanpun juga
            berwarna putih bersih, dan pada jengger ayam ini berwarna merah. Ayam jenis ini
            merupakan ayam yang dimafaatkan hanya pada telur nya saja dan tidak untuk
            memproduksi dagingnya. Selain itu ayam petelur putih ini mampu memproduksi
            telur mencapai 260 butir pertahunnya.
            <br><br>
            Disamping itu ayam petelur putih juga memiliki kelemahan yaitu sangat sensitif
            terhadap cuaca panas dan suasana keributan yang membuat ayam ras ini mudah
            terkejut sehingga dapat berdampak pada jumlah produksi telur yang menurun.
          </p>
          <li>Ayam Lohman Brown</li>
          <img src="image/lohman.jpg" alt="Lohman" height="300">
          <p>
            SJenis ayam petelur lohman merupakan klasifikasi ayam yang diproduksi oleh
            perusahan Miltibreeder Adirama Indonesia. Ayam tersebut memiliki warna bulu
            cokelat seperti karamel dengan warna bulu putih di sekitar leher dan diujung ekor.
            <br><br>
            Menginjak umur ayam 8 minggu ayam lohman broen dapat memproduksi telur
            hingga 300 butir pertahunnya. Ayam jenis ini sangat cocok dijadikan sebagai ayam
            petelur karena produksi telurnya yang lebih banyaj dibanding dengan jenis ayam
            petelur lainnya. Para peternak ayam petelur akan memelihara ayam jenis lohman
            brown ini pada umur fase grower atau fase ayam sudah mulai berpoduksi.
          </p>
        </ol>
      </p>
    </div>
  </div>
</body>
</html>
