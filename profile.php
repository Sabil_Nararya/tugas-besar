<?php

session_start();

if (!isset($_SESSION['username'])) {
  echo "<script>
  alert('Login terlebih dahulu!');
  window.location.href = 'login.php';
  </script>";
}
 
?>


<!DOCTYPE html>
<html>
<head>
	<title>Profile</title>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="style.css">
	<link rel="stylesheet" href="card.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
</head>
<body>

	<nav class="navbar navbar-light bg-info">
		<ul class="nav justify-content-end">

        <li><a href="home.php">Home</a></li>
        <li><a href="jenis-hewan.php">Jenis Hewan</a></li>
        <li><a href="tips-beternak.php">Tips Beternak</a></li>

		<li class="dropdown" style="float:right">
          	<button class="dropbtn"><a href="#">Welcome, <?php echo $_SESSION['username'];  ?></a></button>
          	<div class="dropdown-content">
            	<a href="logout.php">Logout</a>
            	<a href="profile.php">See Profile</a>
            	<a href="author.php">See Author</a>
          	</div>
        </li>
	    </ul>
	</nav>


	<div>
	  <div>
	    <form action="Update.php" method="post">
	    	<div class="row text-center">
				<div style="text-align: center;">
					<h1>Profile</h1>
				</div>
			</div>

	    	<div style="text-align: center;">
				<div>
					<h2>User Anda</h2>
				</div>
			</div>
			<div style="text-align: center; ">
				<div>
					<label style="font-size: 30px">
						<?php echo $_SESSION['username']; ?>
					</label>
				</div>
			</div>
			<br>
			<div style="text-align: center;">
				<div>
					<label style="font-size: 30px">Ganti Username</label>
				</div>
				<div>
					<input type="text" name="username" required placeholder="Contoh aja" class="form-control" value="<?php echo $_SESSION['username']; ?>">
				</div>
			</div>

			<hr>
			<br>

			<div style="text-align: center;">
				<div>
					<label>New Password</label>
				</div>
				<div>
					<input type="password" name="newPassword" required placeholder="New Password" class="form-control">
				</div>
			</div>

			<br>
			<br>

			<div style="text-align: center;">
				<div >
					<label>Confirm Password</label>
				</div>
				<div>
					<input type="password" name="confirmPassword" required placeholder="Confirm Password" class="form-control">
				</div>
				<br>
				<br>
			</div>
			<div style="clear:both" align="center">
        		<a href="update.php"><button class="button-utama">Submit</button></a>
        		<button type="reset" class="button-utama">Cancel</button>
            </div>
            	
				<!-- <div style="alignment-baseline: central;">
					<input type="submit" value="Save" class="form-control">
					<a href="home.php"><input type="submit" value="Cancel"></a>
				</div> -->
	    </form>
	  </div>
	</div>
	<br>
	<br>
	<footer class="footer">
		<div style="text-align: center;">
	    	<div class="footer text-center pt-4">The Real Harvest Moon</div>
      	</div>
    </footer>
</body>

	<link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
	<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
	<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
</html>