<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>Real Harvest Moon</title>
    <link rel="stylesheet" href="style.css">
    <link href="https://fonts.googleapis.com/css?family=Roboto&display=swap" rel="stylesheet">

  </head>
  <body>
    <div class="container">
      <h1>Welcome To Real Harvest Moon</h1>
      <div class="buttons">
        <a href="login.php"><button class="button-utama">Login</button></a>
        <a href="register.php"><button class="button-utama">Register</button></a>
      </div>
    </div>

  </body>
</html>
