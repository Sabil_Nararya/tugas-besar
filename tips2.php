<?php

session_start();

if (!isset($_SESSION['username'])) {
  echo "<script>
  alert('Login terlebih dahulu!');
  window.location.href = 'login.php';
  </script>";
}

?>

<!DOCTYPE html>
<html >
<head>
  <meta charset="utf-8">
  <title>Real Harvest Moon</title>
  <link rel="stylesheet" href="style.css">
  <link href="https://fonts.googleapis.com/css?family=Roboto&display=swap" rel="stylesheet">

</head>
<body>
  <div class="container home">
    <nav class="navbar">
      <ul>
        <li><a href="home.php">Home</a></li>
        <li><a href="jenis-hewan.php">Jenis Hewan</a></li>
        <li><a href="tips-beternak.php">Tips Beternak</a></li>
        <li class="dropdown" style="float:right">
          <button class="dropbtn"><a href="#">Welcome,<?php echo $_SESSION['username'];  ?></a>
          </button>
          <div class="dropdown-content">
            <a href="logout.php">Logout</a>
            <a href="profile.php">See Profile</a>
            <a href="author.php">See Author</a>
          </div>
        </li>
      </ul>
</nav>
    <div>
        <h1 style="color: black">post tips</h1>
            <form action="posttips.php" method="POST">
              <input type="text" name="title" required placeholder="Masukkan Title">
              <br>
              <br>
              <textarea name="post" cols="30" rows="15" required placeholder ="Isi tips disini"></textarea>
                    <br>
                    <br>
                    <input type="text" name="author" required placeholder="Author">
                    <br>
                    <br>
            <a href="posttips.php"><button class="button-utama">Submit</button></a>
    </form>
    </div>
