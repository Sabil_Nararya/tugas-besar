<!DOCTYPE html>
<html lang="en" dir="ltr">
<head>
  <meta charset="utf-8">
  <title>Real Harvest Moon</title>
  <link rel="stylesheet" href="style.css">
  <link href="https://fonts.googleapis.com/css?family=Roboto&display=swap" rel="stylesheet">

</head>
<body>
  <div class="container home">
    <nav class="navbar">
      <ul>
        <li><a href="home.php">Home</a></li>
        <li><a href="jenis-hewan.php">Jenis Hewan</a></li>
        <li><a href="tips-beternak.php">Tips Beternak</a></li>
        <li class="dropdown" style="float:right">
          <button class="dropbtn"><a href="#"></a>
          </button>
          <div class="dropdown-content">
            <a href="logout.php">Logout</a>
            <a href="profile.php">See Profile</a>
            <a href="author.php">See Author</a>
          </div>
        </li>
      </ul>
    </nav>
    <div align= "center">
        <h1>Post Sapi</h1>
          <form action="postsapi.php" method="POST">
            <label>Deskripsi:</label>
            <textarea name="post" cols="40" rows="10">Silahkan isi komentar anda</textarea>
            <br><br>      
            <input type="file" name="image">
            <br><br>
            <input type="submit" name="submit" value="Submit">
          </form>
    </div>
  </div>
</body>
</html>