<?php

session_start();

if (!isset($_SESSION['username'])) {
  echo "<script>
  alert('Login terlebih dahulu!');
  window.location.href = 'login.php';
  </script>";
}

?>


<!DOCTYPE html>
<html lang="en" dir="ltr">
<head>
  <meta charset="utf-8">
  <title>Real Harvest Moon</title>
  <link rel="stylesheet" href="style.css">
  <link href="https://fonts.googleapis.com/css?family=Roboto&display=swap" rel="stylesheet">

</head>
<body>
  <div class="container home">
    <nav class="navbar">
      <ul>
        <li><a href="home.php">Home</a></li>
        <li><a href="jenis-hewan.php">Jenis Hewan</a></li>
        <li><a href="tips-beternak.php">Tips Beternak</a></li>
        <li class="dropdown" style="float:right">
          <button class="dropbtn"><a href="#">Welcome, <?php echo $_SESSION['username'];  ?></a>
          </button>
          <div class="dropdown-content">
            <a href="logout.php">Logout</a>
            <a href="profile.php">See Profile</a>
            <a href="author.php">See Author</a>
          </div>
        </li>
      </ul>
    </nav>
    <div class="konten">
      <h1>What to do ?</h1>
      <p>Sebelum beternak ada 4 hal utama yang harus diketahui terlabih dahulu, yaitu :
        <ol>
          <li>Mengetahui Hewan yang ingin di ternak</li>
          <li>Mengetahui jenis hewan ternak tersebut</li>
          <li>Mengetahui cara merawat hewan ternak tersebut</li>
          <li>Mengetahui hasil ternak yang dihasilkan oleh hewan ternak tersebut</li>
        </ol>
        Setelah mengetahui 4 hal dasar tersebut barulah kita bisa memulai untuk beternak!
      </p>
      <a href="jenis-hewan.php"><button class="button-utama">Mulai Mempelajari Sekarang!</button></a>
    </div>
  </div>
</body>
</html>
