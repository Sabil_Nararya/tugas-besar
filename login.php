<!DOCTYPE html>
<html lang="en" dir="ltr">
<head>
  <meta charset="utf-8">
  <title>Real Harvest Moon</title>
  <link rel="stylesheet" href="style.css">
  <link href="https://fonts.googleapis.com/css?family=Roboto&display=swap" rel="stylesheet">

</head>
<body>
  <div class="container">
    <form class="form" action="login-proses.php" method="post">
      <h2>Login</h2>
      <center>
        <div class="paket">
          <label for="">Username</label><br>
          <input type="text" name="username" value="" class="input-utama">
        </div>
        <div class="paket">
          <label for="">Password</label><br>
          <input type="password" name="password" value="" class="input-utama">
        </div>
      </center>
      <button class="button-utama" type="submit" name="login">Login</button>
    </form>
  </div>
</body>
</html>
