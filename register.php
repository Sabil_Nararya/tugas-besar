<!DOCTYPE html>
<html lang="en" dir="ltr">
<head>
  <meta charset="utf-8">
  <title>Real Harvest Moon</title>
  <link rel="stylesheet" href="style.css">
  <link href="https://fonts.googleapis.com/css?family=Roboto&display=swap" rel="stylesheet">

</head>
<body>
  <div class="container">
    <form class="form" action="daftar-proses.php" method="post">
      <h2>Register</h2>
      <center>
        <div class="paket">
          <label for="">Username</label><br>
          <input type="text" name="username" value="" class="input-utama">
        </div>
        <div class="paket">
          <label for="">Password</label><br>
          <input type="password" name="password" value="" class="input-utama">
        </div>
      </center>
      <button class="button-utama" type="submit" name="daftar">Daftar</button>
    </form>
  </div>
</body>
</html>
